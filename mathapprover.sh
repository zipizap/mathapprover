#!/usr/bin/env bash
# Paulo Aleixo Campos
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${1}"; exit 99; }
trap 'error $LINENO' ERR
#exec > >(tee -i /tmp/$(date +%Y%m%d%H%M%S.%N)__$(basename $0).log ) 2>&1
set -o errexit
  # NOTE: the "trap ... ERR" alreay stops execution at any error, even when above line is commente-out
set -o pipefail
set -o nounset
#set -o xtrace

LOGFILE="${__dir}/$(basename $0 .sh).log"
i_max=5
for i in $(seq 1 $i_max); do
  centenas=$i+1
  # NOTA: RANDOM % 100 => 0..99
  numA=$(( (centenas*100) + (RANDOM % 100) ))
    # 123
  numB=$(( ((centenas-1)*100) + (RANDOM % 100) ))
  resultadoCorrecto=$((numA-numB))
  resultadoInput=0
  while [[ "$resultadoInput" -ne "$resultadoCorrecto" ]]; do
    resultadoInput=$(zenity --entry --title="Desafío $i/$i_max" --text="

  Hola Leo

  Los orcos la han liado parda en el despacho de los Elfos y han hechado a perder su libro de cuentas!

  Necesitamos tu ayuda para volver a calcular $i_max operaciones correctas, y así reponer orden en el libro de cuentas de los Elfos
  
  Las cuentas se necesitan bien hechas, para calcular cuantas manzanas tienen y cuantas les falta - si no salen bien, igual algun
  caballo se queda sin su postre favorito :) 


  Esta es la operacion $i/$i_max  - saca el boli y una hoja, y mucha suerte ;)

        $numA
     -  $numB
     --------
        ?????

" --entry-text "?resultado?" 2>/dev/null)
    # log
    echo "[$(date +%Y%m%d%H%M%S)]    $numA - $numB = '$resultadoInput'" >> $LOGFILE

    # clean leading-zeros (to avoid confusions with octal values)
    resultadoInput=$(echo $resultadoInput | sed 's/^0//g')
    # assure only digits pass, or set unplausible-value-to-force-a-failure
    if ! [[ $resultadoInput =~ ^[[:digit:]]+$ ]]
    then
      resultadoInput="999999999"
    fi
  done
done
exit 0
